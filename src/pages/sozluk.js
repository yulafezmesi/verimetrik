import React from "react"
import Layout from "../components/layouts/secondlayout"
import SEO from "../components/seo"
import "../style/main.scss"
import { useIntl } from "gatsby-plugin-intl"
import GlossaryItems from "../components/glossaryitems"
const IndexPage = () => {
  const { locale, formatMessage } = useIntl()
  return (
    <Layout>
      <SEO title={formatMessage({ id: "title" })} lang={locale} />
      <main className="p-lg-4 pt-3">
        <GlossaryItems />
      </main>
    </Layout>
  )
}

export default IndexPage
