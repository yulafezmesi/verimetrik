import React from "react"
import Layout from "../components/layouts/secondlayout"
import SEO from "../components/Seo"
import { useIntl } from "gatsby-plugin-intl"

const NotFoundPage = () => {
  const { locale, formatMessage } = useIntl()

  return (
    <Layout>
      <SEO
        description={formatMessage({ id: "notfound.description" })}
        title={formatMessage({ id: "notfound.header" })}
        lang={locale}
      />
      <div className="container-404 container-404-content">
        <div className="container-404-content">
          <span className="container-404-content-text">
            40<span className="container-404-content-broken">4</span>
          </span>
          <p className="container-404-content-title">
            {formatMessage({ id: "notfound.description" })}
          </p>
        </div>
      </div>
    </Layout>
  )
}

export default NotFoundPage
