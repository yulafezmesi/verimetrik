import React from "react"
import Layout from "../components/layouts/mainlayout"
import SEO from "../components/seo"
import "../style/main.scss"
import { useIntl } from "gatsby-plugin-intl"
import Section3 from "../components/section-3"
import Section2 from "../components/section-2"
import Section4 from "../components/section-4"
import Section5 from "../components/section-5"
import Section6 from "../components/section-6"
import Section7 from "../components/section-7"
import Section8 from "../components/section-8"
import Section9 from "../components/section-9"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Main from "../components/mainsection"
const PrevButton = ({ onClick }) => {
  return (
    <button
      onClick={onClick}
      className="slick-prev slick-white"
      style={{ left: 0, zIndex: 3, color: "black" }}
    >
      Prev
    </button>
  )
}

const NextButton = ({ onClick }) => {
  return (
    <button
      onClick={onClick}
      className="slick-next slick-white"
      style={{ right: 0, zIndex: 3, color: "white !important" }}
    >
      Next
    </button>
  )
}
const IndexPage = () => {
  const settings = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: <PrevButton />,
    nextArrow: <NextButton />,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  }
  const { locale, formatMessage } = useIntl()
  return (
    <Layout>
      <SEO title={formatMessage({ id: "title" })} lang={locale} />
      <Section2 />
      <Main>
        <Section3 />
        <Section4 />
        <Section5 />
        <Section6 />
        <Section7 />
        <Section8 />
        <Section9 />
      </Main>
    </Layout>
  )
}

export default IndexPage
