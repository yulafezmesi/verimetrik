import React from "react"
import {
  WhatsApp,
  Facebook,
  LinkedIn,
  Twitter,
  Instagram,
  YouTube,
  Mail,
} from "@material-ui/icons"

function socials() {
  return (
    <>
      <a alt="Twitter" href="https://twitter.com/verimetrik" target="_blank">
        <Twitter fontSize="small" />
      </a>
      <a
        alt="Instagram"
        href="https://www.instagram.com/verimetrik"
        target="_blank"
      >
        <Instagram fontSize="small" />
      </a>

      <a
        target="_blank"
        alt="YouTube"
        href="https://www.youtube.com/VerimetrikTR"
      >
        <YouTube />
      </a>
      <a
        alt="LinkedIn"
        href="https://www.linkedin.com/company/verimetrik/"
        target="_blank"
      >
        <LinkedIn fontSize="small" />
      </a>
      <a
        target="_blank"
        alt="Facebook"
        href="https://www.facebook.com/verimetrik"
      >
        <Facebook fontSize="small" />
      </a>
      <a target="_blank" alt="WhatsApp" href="https://wa.me/902166932300">
        <WhatsApp fontSize="small" />
      </a>
      <a target="_blank" alt="MailTo" href="mailto:info@verimetrik.com">
        <Mail fontSize="small" />
      </a>
    </>
  )
}

export default socials
