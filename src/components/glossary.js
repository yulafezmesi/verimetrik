import React, { useEffect, useState, useContext } from "react"
import SEO from "./seo"
import "../style/main.scss"
import { navigate, Link } from "gatsby"

import { MainContext } from "../contexts/maincontext"
const GlossaryItems = ({ id, visibleBottomRef }) => {
  const { glossary, locale, titles } = useContext(MainContext)
  const [glossaryDetail, setGlossaryDetail] = useState({})
  const [linkedTitles, setLinkedTitles] = useState([])

  useEffect(() => {
    const detail = glossary.find(item => item.URLID === id)
    if (!detail) navigate(`${locale}/404/`)
    try {
      let titleIdArray = detail["İlişkili-Veriler"].split(",")
      let titles = []
      titleIdArray.map(item => {
        if (detail.ID != item) {
          let title = glossary.find(item2 => item2.ID == item)
          if (title != undefined) {
            titles.push(title)
          }
        }
      })
      setLinkedTitles(titles.sort((a, b) => a.URLID.localeCompare(b.URLID)))
    } catch (e) {}
    setGlossaryDetail(detail)
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      setTimeout(() => {
        visibleBottomRef.current.scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "start",
        })
      }, 800)
    }
  }, [id])
  const htmlToElement = html => {
    var template = document.createElement("template")
    if (html) {
      html = html.trim() // Never return a text node of whitespace as the result
      template.innerHTML = html
      return template.content.firstChild.src.replace("interactive", "image")
    } else {
      return null
    }
  }
  return glossaryDetail ? (
    <>
      <SEO
        meta={glossaryDetail.Metatag}
        description={glossaryDetail.Description}
        lang={locale}
      />
      <div className="glossary-content-container">
        <h2>{glossaryDetail.Başlık}</h2>
        <div className="d-flex flex-wrap justify-content-between mb-3 ">
          <table>
            {glossaryDetail["Yayınlayan-Kurum"] ? (
              <tr>
                <td valign="top" className="glossary-content-container-title">
                  <span style={{ fontSize: "1rem" }}>
                    {titles["Yayınlayan-Kurum"]}
                  </span>
                  <span className="glossary-content-container-title-dots">
                    :
                  </span>
                </td>
                {glossaryDetail["Yayınlayan-Kurum"] ? (
                  <td className="glossary-content-container-td">
                    {glossaryDetail["Yayınlayan-Kurum"]}
                  </td>
                ) : null}
              </tr>
            ) : null}
            {glossaryDetail["İlk-Yayınlanma-Tarihi"] ? (
              <tr>
                <td valign="top" className="glossary-content-container-title">
                  <span style={{ fontSize: "1rem" }}>
                    {titles["İlk-Yayınlanma-Tarihi"]}
                  </span>
                  <span className="glossary-content-container-title-dots">
                    :
                  </span>
                </td>
                <td className="glossary-content-container-td">
                  {glossaryDetail["İlk-Yayınlanma-Tarihi"]}
                </td>
              </tr>
            ) : null}
            {glossaryDetail.Periyodu ? (
              <tr>
                <td valign="top" className="glossary-content-container-title">
                  <span style={{ fontSize: "1rem" }}>{titles["Periyodu"]}</span>
                  <span className="glossary-content-container-title-dots">
                    :
                  </span>
                </td>
                <td className="glossary-content-container-td">
                  {glossaryDetail.Periyodu}
                </td>
              </tr>
            ) : null}
          </table>
          {glossaryDetail["Önem-Derecesi"] ? (
            <img
              alt={glossaryDetail.Başlık}
              src={`/images/glossary/risk/risk-${glossaryDetail["Önem-Derecesi"]}.png`}
            />
          ) : null}
        </div>
      </div>
      {glossaryDetail.Açıklama ? (
        <>
          <h4>{titles["Açıklama"]}</h4>
          <p>{glossaryDetail.Açıklama}</p>
        </>
      ) : null}

      {glossaryDetail.CHURL ? (
        <>
          <h4 className="mb-md-0">{titles["Grafik"]}</h4>
          <div className="chart-wrapper-is-mobile">
            <img src={htmlToElement(glossaryDetail.CHURL)} />
          </div>
          <div
            className="chart-wrapper"
            dangerouslySetInnerHTML={{ __html: glossaryDetail.CHURL }}
          ></div>
          {glossaryDetail["Grafik-Nasıl-Okunmalı"] ? (
            <>
              <h4 style={{ marginTop: "5.8em " }}>
                {titles["Grafik-Nasıl-Okunmalı"]}
              </h4>
              <p>{glossaryDetail["Grafik-Nasıl-Okunmalı"]}</p>
            </>
          ) : null}
        </>
      ) : null}
      {glossaryDetail["Neyi-Nasıl-Etkiler"] ? (
        <>
          <h4> {titles["Neyi-Nasıl-Etkiler"]}</h4>
          <p>{glossaryDetail["Neyi-Nasıl-Etkiler"]}</p>
        </>
      ) : null}
      {glossaryDetail["Kullanım-Şekli"] ? (
        <>
          <h4> {titles["Kullanım-Şekli"]}</h4>
          <p>{glossaryDetail["Kullanım-Şekli"]}</p>
        </>
      ) : null}
      {linkedTitles && glossaryDetail["İlişkili-Veriler"] ? (
        <>
          <h4>{titles["İlişkili-Veriler"]}</h4>
          <ul>
            {linkedTitles.map((item, i) => (
              <li key={item.ID}>
                <Link to={`/${locale}/sozluk/${item.URLID}`}>
                  {item.Başlık}
                </Link>
              </li>
            ))}
          </ul>
        </>
      ) : null}
      {glossaryDetail.YTURL ? (
        <div className="embed-container">
          <iframe
            src={glossaryDetail.YTURL}
            frameborder="0"
            allowfullscreen
          ></iframe>
        </div>
      ) : null}
    </>
  ) : null
}

export default GlossaryItems
