import React, { useContext, useState } from "react"
import Slider from "react-slick"
import { MainContext } from "../contexts/maincontext"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

const PrevButton = ({ onClick }) => {
  return (
    <button
      onClick={onClick}
      className="slick-prev"
      style={{ left: 0, zIndex: 3, color: "black" }}
    >
      Prev
    </button>
  )
}

const NextButton = ({ onClick }) => {
  return (
    <button
      onClick={onClick}
      className="slick-next"
      style={{ right: 0, zIndex: 3, color: "black !important" }}
    >
      Next
    </button>
  )
}

export default function Section8() {
  const { getFilteredContent, getBrandList } = useContext(MainContext)

  var settings = {
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow: <PrevButton />,
    nextArrow: <NextButton />,
    autoplay: true,
    autoplaySpeed: 2000,
  }
  return (
    <section className="section-9 pl-lg-5 pr-lg-5" id="section-9">
      <div className="section-9-container">
        <h2>{getFilteredContent("section-9", "1", "Title")}</h2>
        <span> {getFilteredContent("section-9", "1", "Text")}</span>
        <hr className="divider divider-bold" />
      </div>
      <Slider {...settings}>
        {getBrandList().map(item => (
          <>
            <div className="section-9-item">
              <img alt={item} src={`/images/client/${item}.png`} />
            </div>
          </>
        ))}
      </Slider>
    </section>
  )
}
