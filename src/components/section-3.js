import React, { useContext } from "react"
import { MainContext } from "../contexts/maincontext"
const Third = () => {
  const { getFilteredContent } = useContext(MainContext)
  return (
    <section id="section-3">
      <div className="container-third border-shadow p10">
        <div className="container-third-first">
          <h3
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-3", "1", "Title"),
            }}
          ></h3>
          <p
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-3", "1", "Text"),
            }}
          ></p>
        </div>
        <div className="d-flex d-flex-column container-third-second">
          <h3
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-3", "2", "Title"),
            }}
          ></h3>
          <p
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-3", "2", "Text"),
            }}
          ></p>
        </div>
        <div className="d-flex d-flex-column ">
          <h3
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-3", "3", "Title"),
            }}
          ></h3>
          <p
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-3", "3", "Text"),
            }}
          ></p>
        </div>
      </div>
    </section>
  )
}

export default Third
