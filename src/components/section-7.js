import React, { useContext } from "react"
import Benefits7 from "./images/benefits-7"
import Benefits8 from "./images/benefits-8"
import Benefits9 from "./images/benefits-9"
import { MainContext } from "../contexts/maincontext"
const Seventh = () => {
  const { getFilteredContent } = useContext(MainContext)
  return (
    <section>
      <div className="section-7">
        <div className="container-seventh">
          <div className="container-seventh-first border-shadow">
            <Benefits7 />
            <hr className="divider divider-bold" />
            <p
              dangerouslySetInnerHTML={{
                __html: getFilteredContent("section-7", "1", "Text"),
              }}
            ></p>
          </div>
          <div className="container-seventh-first border-shadow">
            <Benefits8 />
            <hr className="divider divider-bold" />
            <p
              dangerouslySetInnerHTML={{
                __html: getFilteredContent("section-7", "2", "Text"),
              }}
            ></p>
          </div>
          <div className="container-seventh-first border-shadow">
            <Benefits9 />
            <hr className="divider divider-bold" />
            <p
              dangerouslySetInnerHTML={{
                __html: getFilteredContent("section-7", "3", "Text"),
              }}
            ></p>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Seventh
