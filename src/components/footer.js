import React, { useContext } from "react"
import Socials from "./socials"
import { Link } from "gatsby"
import { changeLocale } from "gatsby-plugin-intl"
import { MainContext } from "../contexts/maincontext"
function Section() {
  const { locale, getFilteredContent } = useContext(MainContext)
  return (
    <footer id="footer" className="footer">
      <div className="grid justify-content-center flex-wrap d-flex grid-pad pl-lg-5 pr-lg-5">
        <div className="footer-contact  col-2-12 d-flex flex-column align-items-center justify-content-center  ">
          <div className="footer-logo mb-1">
            <a href={`/${locale}`}>
              <img alt="Verimetrik Logo" src={`/images/logo-white.png`} />
            </a>
          </div>

          <div className="footer-menu">
            <a href={`/${locale}`}>
              {getFilteredContent("header", "1", "Text")}
            </a>
            <Link>{getFilteredContent("header", "2", "Text")}</Link>
            <Link to={`${locale}/sozluk/`}>
              {getFilteredContent("header", "3", "Text")}
            </Link>
            {getFilteredContent("header", "4", "SwitchLang", "tr") === "ON" ? (
              <a
                onClick={() => changeLocale(locale == "en" ? "tr" : "en")}
                className="nav-container-menu-lang"
              >
                {locale == "en" ? "TR" : "EN"}
              </a>
            ) : null}
            <div className="footer-social">
              <Socials />
            </div>
            <a target="_blank" href="tel:+90 216 693 2300">
              +90 216 693 2300
            </a>
            <a target="_blank" href="mailto:info@verimetrik.com">
              info@verimetrik.com
            </a>
          </div>
        </div>
        <div className="col-5-12 pt-4  d-flex flex-column justify-content-around">
          <h3>{getFilteredContent("footer", "2", "Title")}</h3>
          <p
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("footer", "2", "Location"),
            }}
          ></p>
          <div
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("footer", "2", "Map", "tr"),
            }}
          ></div>
        </div>
        <div className="col-5-12 pt-4 footer-rmpad d-flex flex-column justify-content-around">
          <h3>{getFilteredContent("footer", "3", "Title")}</h3>
          <p
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("footer", "3", "Location"),
            }}
          ></p>
          <div
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("footer", "3", "Map", "tr"),
            }}
          ></div>
        </div>
      </div>
      <div className="footer-bottom pt-3 pb-3">
        <span
          dangerouslySetInnerHTML={{
            __html: getFilteredContent("footer", "4", "Text", "tr"),
          }}
        ></span>
      </div>
    </footer>
  )
}

export default Section
