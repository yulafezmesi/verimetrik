import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const Image = () => {
  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "benefits-2.png" }) {
        childImageSharp {
          fluid(maxWidth: 300, maxHeight: 200) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <Img
      imgStyle={{ padding: "10px" }}
      fluid={data.file.childImageSharp.fluid}
    />
  )
}

export default Image
