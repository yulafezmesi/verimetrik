import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const Image = () => {
  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "benefits-5.png" }) {
        childImageSharp {
          fluid (maxWidth:200,maxHeight:200) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <div  data-sal="flip-up">
      <Img fluid={data.file.childImageSharp.fluid} />
    </div>
  )
}

export default Image
