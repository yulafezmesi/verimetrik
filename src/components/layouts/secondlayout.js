import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Header from "../headers/secondheader"
import MainContextProvider from "../../contexts/maincontext"
import "../../style/layout.css"
import "../../style/customlayout.scss"
import Footer from "../footer"
const SecondLayout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery2 {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <MainContextProvider>
      <Header siteTitle={data.site.siteMetadata.title} />
      {children}
      <Footer></Footer>
    </MainContextProvider>
  )
}

SecondLayout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default SecondLayout
