import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Header from "../headers/mainheader"
import MainContextProvider from "../../contexts/maincontext"
import "../../style/layout.css"
import "../../style/customlayout.scss"
import Footer from "../footer"
const MainLayout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <MainContextProvider>
      <Header siteTitle={data.site.siteMetadata.title} />
      <section>{children}</section>
      <Footer></Footer>
    </MainContextProvider>
  )
}

MainLayout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default MainLayout
