import React, { useContext } from "react"

import { MainContext } from "../contexts/maincontext"

const Fifth = () => {
  const { getFilteredContent } = useContext(MainContext)

  return (
    <section id="section-5">
      <div className="container-fifth">
        <div style={{ maxWidth: "500px" }}>
          <img alt="benefits-5" src={`/images/benefits-5.png`} />
        </div>

        <div className="container-fifth-second">
          <h3
            data-sal="zoom-in"
            data-sal-delay="600"
            style={{
              fontStyle: "italic",
              fontWeight: 200,
              color: "#ff414a",
              textAlign: "center",
            }}
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-5", "1", "Title"),
            }}
          ></h3>
          <div className="container-fifth-second-list">
            <div
              data-sal="zoom-in"
              data-sal-delay="600"
              className="container-fifth-second-list-first"
            >
              <div className="d-flex d-flex-row d-flex-align-center">
                <span
                  dangerouslySetInnerHTML={{
                    __html: getFilteredContent("section-5", "2", "Title"),
                  }}
                  style={{ fontSize: "18px", fontWeight: "bold" }}
                ></span>
              </div>
              <p
                dangerouslySetInnerHTML={{
                  __html: getFilteredContent("section-5", "2", "Text"),
                }}
              ></p>
            </div>
            <div
              data-sal="zoom-in"
              data-sal-delay="600"
              className="container-fifth-second-list-second"
            >
              <div className="d-flex d-flex-row d-flex-align-center">
                <span
                  dangerouslySetInnerHTML={{
                    __html: getFilteredContent("section-5", "3", "Title"),
                  }}
                  style={{ fontSize: "18px", fontWeight: "bold" }}
                ></span>
              </div>
              <p
                dangerouslySetInnerHTML={{
                  __html: getFilteredContent("section-5", "3", "Text"),
                }}
              ></p>
            </div>
            <div
              data-sal="zoom-in"
              data-sal-delay="600"
              className="container-fifth-second-list-third"
            >
              <div className="d-flex d-flex-row d-flex-align-center">
                <span
                  dangerouslySetInnerHTML={{
                    __html: getFilteredContent("section-5", "4", "Title"),
                  }}
                  style={{ fontSize: "18px", fontWeight: "bold" }}
                ></span>
              </div>
              <p
                dangerouslySetInnerHTML={{
                  __html: getFilteredContent("section-5", "4", "Text"),
                }}
              ></p>
            </div>
            <div
              data-sal="zoom-in"
              data-sal-delay="600"
              className="container-fifth-second-list-third"
            >
              <div className="d-flex d-flex-row d-flex-align-center">
                <span
                  dangerouslySetInnerHTML={{
                    __html: getFilteredContent("section-5", "5", "Title"),
                  }}
                  style={{ fontSize: "18px", fontWeight: "bold" }}
                ></span>
              </div>
              <p
                dangerouslySetInnerHTML={{
                  __html: getFilteredContent("section-5", "5", "Text"),
                }}
              ></p>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Fifth
