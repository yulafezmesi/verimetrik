import PropTypes from "prop-types"
import React, { useState, useContext } from "react"
import { Link } from "gatsby"
import HamburgerMenu from "react-hamburger-menu"
import Socials from "../socials"
import { changeLocale } from "gatsby-plugin-intl"
import { MainContext } from "../../contexts/maincontext"

const Header = ({ siteTitle }) => {
  const { locale, getFilteredContent, isLoading } = useContext(MainContext)
  const [open, setOpen] = useState(false)

  return (
    <div>
      {!isLoading ? (
        <nav className="scroll-down">
          <div className="nav">
            <div className="nav-container">
              <div className="nav-container-logo">
                <a href={`/${locale}`}>
                  <img alt="Verimetrik Logo" src={`/images/logo.png`} />
                </a>
              </div>
              <div className="nav-container-menu">
                <a href={`/${locale}`}>
                  {getFilteredContent("header", "1", "Text")}
                </a>
                <Link to={`${locale}/`}>
                  {getFilteredContent("header", "2", "Text")}
                </Link>
                <Link to={`${locale}/sozluk/`}>
                  {getFilteredContent("header", "3", "Text")}
                </Link>
                {getFilteredContent("header", "4", "SwitchLang", "tr") ===
                "ON" ? (
                  <a
                    onClick={() => changeLocale(locale == "en" ? "tr" : "en")}
                    className="nav-container-menu-lang"
                  >
                    {locale == "en" ? "TR" : "EN"}
                  </a>
                ) : null}
                <div className="nav-container-menu-social color-white ">
                  <Socials />
                </div>
              </div>

              <div className="nav-container-hamburger">
                <HamburgerMenu
                  isOpen={open}
                  menuClicked={() => {
                    setOpen(!open)
                  }}
                  width={26}
                  height={26}
                  strokeWidth={2}
                  rotate={0}
                  color="#113377"
                  borderRadius={0}
                  transition="none"
                ></HamburgerMenu>
                <div
                  className={
                    open
                      ? "nav-container-hamburger-open"
                      : `nav-container-hamburger-open nav-container-hamburger-close`
                  }
                >
                  <ul>
                    <a>{getFilteredContent("header", "1", "Text")}</a>
                    <Link>{getFilteredContent("header", "2", "Text")}</Link>
                    <Link to={`${locale}/sozluk/`}>
                      {getFilteredContent("header", "3", "Text")}
                    </Link>
                    {getFilteredContent("header", "4", "SwitchLang", "tr") ===
                    "ON" ? (
                      <a
                        onClick={() =>
                          changeLocale(locale == "en" ? "tr" : "en")
                        }
                        className="nav-container-menu-lang"
                      >
                        {locale == "en" ? "TR" : "EN"}
                      </a>
                    ) : null}
                    <div className="nav-container-menu-social">
                      <Socials />
                    </div>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </nav>
      ) : (
        <div className="loading">
          <div className="loading-loader"></div>
        </div>
      )}
    </div>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
