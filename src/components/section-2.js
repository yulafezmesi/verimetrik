import React, { useContext, useState } from "react"
import { CSSTransition } from "react-transition-group"

import { MainContext } from "../contexts/maincontext"
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
const PrevButton = ({ onClick }) => {
  return (
    <button
      onClick={onClick}
      className="slick-prev slick-white"
      style={{ left: 0, zIndex: 3, color: "black" }}
    >
      Prev
    </button>
  )
}

const NextButton = ({ onClick }) => {
  return (
    <button
      onClick={onClick}
      className="slick-next slick-white"
      style={{ right: 0, zIndex: 3, color: "white !important" }}
    >
      Next
    </button>
  )
}

const Second = () => {
  const [increment, setIncrement] = useState(1)
  const { getFilteredContent, isLoading } = useContext(MainContext)
  const settings = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: <PrevButton />,
    nextArrow: <NextButton />,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  }
  return (
    <section className="section-2" id="section-2">
      <div className="container-second-text">
        {!isLoading ? (
          <h1
            dangerouslySetInnerHTML={{
              __html: `${getFilteredContent(
                `section-2`,
                increment.toString(),
                "Text"
              )}`,
            }}
          ></h1>
        ) : null}
      </div>
      <Slider
        afterChange={() => {
          if (increment < 3) {
            setIncrement(increment + 1)
          } else {
            setIncrement(1)
          }
        }}
        {...settings}
      >
        <div>
          <div class="hero is-fullheight  has-background">
            <img alt="Main Bg" class="hero-background is-transparent" src="/images/mainbg.jpg" />

          </div>
        </div>
        <div>
          <div class="hero is-fullheight  has-background">
            <img alt="Main Bg" class="hero-background is-transparent" src="/images/mainbg-2.jpg" />

          </div>
        </div>
        <div>
          <div class="hero is-fullheight  has-background">
            <img alt="Main Bg" class="hero-background is-transparent" src="/images/mainbg-3.jpg" />

          </div>
        </div>
      </Slider>
    </section >
  )
}

export default Second
