import React from "react"
import PropTypes from "prop-types"
import "../style/main.scss"

const MainSection = ({ children }) => {
  return <main>{children}</main>
}

MainSection.propTypes = {
  children: PropTypes.node.isRequired,
}

export default MainSection
