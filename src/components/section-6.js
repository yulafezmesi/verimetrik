import React, { useContext } from "react"
import "../style/main.scss"
import { MainContext } from "../contexts/maincontext"

const Fifth = () => {
  const { offset, getFilteredContent } = useContext(MainContext)

  return (
    <section
      id="section-6"
      style={{ backgroundPositionY: `${offset * 0.6}px` }}
      className="section-6 parallax"
    >
      <div className="section-6-container">
        <h1
          dangerouslySetInnerHTML={{
            __html: getFilteredContent("section-6", "1", "Title"),
          }}
        ></h1>
        <hr className="divider" />
        <p
          dangerouslySetInnerHTML={{
            __html: getFilteredContent("section-6", "1", "Text"),
          }}
          className="section-6-container-text"
        ></p>
      </div>
    </section>
  )
}

export default Fifth
