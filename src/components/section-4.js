import React, { useContext } from "react"
import Benefits3 from "./images/benefits-3"
import { MainContext } from "../contexts/maincontext"

const Fourth = () => {
  const { getFilteredContent } = useContext(MainContext)
  return (
    <section id="section-4">
      <div data-aos="fade-left">
        <h1
          dangerouslySetInnerHTML={{
            __html: getFilteredContent("section-4", "1", "Title"),
          }}
          className="text-align-center "
        ></h1>
        <p
          dangerouslySetInnerHTML={{
            __html: getFilteredContent("section-4", "1", "Text"),
          }}
          className="text-align-center"
        ></p>
      </div>
      <div className="container-fourth">
        <div
          data-aos="zoom-in"
          className="container-fourth-first border-shadow"
        >
          <h2
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-4", "2", "Title"),
            }}
          ></h2>
          <p
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-4", "2", "Text"),
            }}
          ></p>
          <img alt="benefits-3" src="/images/benefits-3.png" />
        </div>
        <div
          data-aos="zoom-in"
          className="container-fourth-second border-shadow"
        >
          <h2
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-4", "3", "Title"),
            }}
          ></h2>
          {/* <Benefits4 /> */}

          <p
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-4", "3", "Text"),
            }}
          ></p>
          <img alt="benefits-4" src="/images/benefits-4.png" />
        </div>
        <div
          data-aos="zoom-in"
          className="container-fourth-third border-shadow"
        >
          <h2
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-4", "4", "Title"),
            }}
          ></h2>
          <p
            dangerouslySetInnerHTML={{
              __html: getFilteredContent("section-4", "4", "Text"),
            }}
          ></p>

          <img alt="benefits-12" src="/images/benefits-12.png" />
        </div>
      </div>
    </section>
  )
}

export default Fourth
