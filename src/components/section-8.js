import React, { useContext } from "react"
import CountUp from "react-countup"
import { MainContext } from "../contexts/maincontext"

export default function Section8() {
  const { section8isVisible, getFilteredContent } = useContext(MainContext)

  return (
    <section id="section-8">
      {section8isVisible ? (
        <div className="container-eighth">
          <div className="container-eighth-countup">
            <div className="container-eighth-countup-box">
              <CountUp
                duration={4}
                delay={1}
                start={0}
                separator="."
                end={+getFilteredContent("section-8", "1", "Count", "tr")}
              ></CountUp>
              <p
                dangerouslySetInnerHTML={{
                  __html: getFilteredContent("section-8", "1", "Text"),
                }}
              ></p>
            </div>
            <div className="container-eighth-countup-box">
              <CountUp
                duration={4}
                delay={1}
                start={0}
                separator="."
                end={+getFilteredContent("section-8", "2", "Count", "tr")}
              ></CountUp>
              <p
                dangerouslySetInnerHTML={{
                  __html: getFilteredContent("section-8", "2", "Text"),
                }}
              ></p>
            </div>
            <div className="container-eighth-countup-box">
              <CountUp
                duration={4}
                delay={1}
                start={0}
                separator="."
                end={+getFilteredContent("section-8", "3", "Count", "tr")}
              ></CountUp>
              <p
                dangerouslySetInnerHTML={{
                  __html: getFilteredContent("section-8", "3", "Text"),
                }}
              ></p>
            </div>
          </div>
        </div>
      ) : (
        <></>
      )}
    </section>
  )
}
