var plugins = [{
      plugin: require('C:/Users/mcertel/Desktop/veri/verimetrik/node_modules/gatsby-plugin-google-analytics/gatsby-ssr'),
      options: {"plugins":[],"trackingId":"UA-3234260-6","head":true},
    },{
      plugin: require('C:/Users/mcertel/Desktop/veri/verimetrik/node_modules/gatsby-plugin-yandex-metrica/gatsby-ssr'),
      options: {"plugins":[],"trackingId":"27051862","clickmap":true,"trackLinks":true,"accurateTrackBounce":true,"trackHash":true,"webvisor":true},
    },{
      plugin: require('C:/Users/mcertel/Desktop/veri/verimetrik/node_modules/gatsby-plugin-react-helmet/gatsby-ssr'),
      options: {"plugins":[]},
    },{
      plugin: require('C:/Users/mcertel/Desktop/veri/verimetrik/node_modules/gatsby-plugin-manifest/gatsby-ssr'),
      options: {"plugins":[],"name":"Verimetrik","short_name":"Verimetrik","start_url":"/tr","background_color":"#113377","theme_color":"#113377","display":"minimal-ui","icon":"src/images/favicon.ico","cache_busting_mode":"query","include_favicon":true,"legacy":true,"theme_color_in_head":true,"cacheDigest":"6c9ef2a42c56f90f192e6c72c8c1ee58"},
    },{
      plugin: require('C:/Users/mcertel/Desktop/veri/verimetrik/node_modules/gatsby-plugin-intl/gatsby-ssr'),
      options: {"plugins":[],"path":"C:\\Users\\mcertel\\Desktop\\veri\\verimetrik/src/intl","languages":["en","tr"],"defaultLanguage":"tr","redirect":true},
    }]
// During bootstrap, we write requires at top of this file which looks like:
// var plugins = [
//   {
//     plugin: require("/path/to/plugin1/gatsby-ssr.js"),
//     options: { ... },
//   },
//   {
//     plugin: require("/path/to/plugin2/gatsby-ssr.js"),
//     options: { ... },
//   },
// ]

const apis = require(`./api-ssr-docs`)

// Run the specified API in any plugins that have implemented it
module.exports = (api, args, defaultReturn, argTransform) => {
  if (!apis[api]) {
    console.log(`This API doesn't exist`, api)
  }

  // Run each plugin in series.
  // eslint-disable-next-line no-undef
  let results = plugins.map(plugin => {
    if (!plugin.plugin[api]) {
      return undefined
    }
    const result = plugin.plugin[api](args, plugin.options)
    if (result && argTransform) {
      args = argTransform({ args, result })
    }
    return result
  })

  // Filter out undefined results.
  results = results.filter(result => typeof result !== `undefined`)

  if (results.length > 0) {
    return results
  } else {
    return [defaultReturn]
  }
}
